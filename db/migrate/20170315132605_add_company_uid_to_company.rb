class AddCompanyUidToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :company_uid, :uuid
  end
end
