class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :issue_type
      t.string :issue_subject
      t.string :issue_description
      t.integer :raised_by
      t.string :raised_at
      t.string :datetime
      t.string :status
      t.integer :assigned_to

      t.timestamps null: false
    end
  end
end
