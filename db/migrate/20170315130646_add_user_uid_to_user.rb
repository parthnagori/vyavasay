class AddUserUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_uid, :uuid
  end
end
