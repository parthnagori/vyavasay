class AddRaisedAtToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :raised_at, :datetime
  end
end
