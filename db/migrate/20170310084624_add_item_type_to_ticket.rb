class AddItemTypeToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :item_type, :string
  end
end
