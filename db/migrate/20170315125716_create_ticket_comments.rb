class CreateTicketComments < ActiveRecord::Migration
  def change
    create_table :ticket_comments do |t|
      t.string :description
      t.integer :commented_by
      t.datetime :commented_at
      t.integer :ticket_id

      t.timestamps null: false
    end
  end
end
