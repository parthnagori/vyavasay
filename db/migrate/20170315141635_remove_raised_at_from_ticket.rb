class RemoveRaisedAtFromTicket < ActiveRecord::Migration
  def change
    remove_column :tickets, :raised_at, :string
    remove_column :tickets, :datetime, :string
  end
end
