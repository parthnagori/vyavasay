class CreateTicketReplies < ActiveRecord::Migration
  def change
    create_table :ticket_replies do |t|
      t.string :description
      t.integer :replied_by
      t.datetime :replied_at
      t.integer :ticket_id
      t.integer :replied_to

      t.timestamps null: false
    end
  end
end
