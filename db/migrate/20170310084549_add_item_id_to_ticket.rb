class AddItemIdToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :item_id, :integer
  end
end
