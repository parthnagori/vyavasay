class User < ActiveRecord::Base
  has_many :tickets
  has_many :ticket_replies
  has_many :ticket_comments, foreign_key: :commented_by
  belongs_to :company
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
