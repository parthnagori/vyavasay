class Ticket < ActiveRecord::Base
  has_many :ticket_replies, :dependent => :destroy
  has_many :ticket_comments, :dependent => :destroy
  has_many :children, :class_name => "Ticket", :foreign_key => :parent_id
  belongs_to :company
  belongs_to :raised_by, :class_name => "User"
  belongs_to :assigned_to, :class_name => "User"
end
