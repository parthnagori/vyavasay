class TicketReply < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :replied_by, , :class_name => "User"
  belongs_to :replied_to, , :class_name => "User"
end
