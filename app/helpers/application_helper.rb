module ApplicationHelper
	def hidden_if_empty?(text)
    "hide-me" if text.blank?
  end

  def apply_if_true(x, class_name, other_name = nil)
    x ? class_name : other_name
  end

  def if_current(path, class_name)
    current_page?(path) ? class_name : nil
  end
end
