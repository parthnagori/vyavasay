json.extract! ticket, :id, :issue_type, :issue_subject, :issue_description, :raised_by, :raised_at, :datetime, :status, :assigned_to, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
