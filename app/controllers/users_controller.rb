class UsersController < ApplicationController


  def index
  end

  def edit
  end

  def update
  end

  def welcome
    if user_signed_in?
      redirect_to dashboard_users_path
    else
      redirect_to new_session_path(User.new)
    end
  end

  def dashboard
  end

end
