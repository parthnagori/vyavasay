Rails.application.routes.draw do
  resources :companies
  resources :tickets
  # root to: 'visitors#index'
  devise_for :users
  
  root :to => "users#welcome"

  resources :users do
    collection do
      get 'dashboard'
    end
  end
end
